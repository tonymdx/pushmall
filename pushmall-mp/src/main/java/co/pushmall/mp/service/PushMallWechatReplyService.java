package co.pushmall.mp.service;


import co.pushmall.mp.domain.PushMallWechatReply;
import co.pushmall.mp.service.dto.PushMallWechatReplyDTO;
import co.pushmall.mp.service.dto.PushMallWechatReplyQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-10-10
 */
//@CacheConfig(cacheNames = "yxWechatReply")
public interface PushMallWechatReplyService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallWechatReplyQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallWechatReplyDTO> queryAll(PushMallWechatReplyQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallWechatReplyDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallWechatReplyDTO create(PushMallWechatReply resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallWechatReply resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);

    PushMallWechatReply isExist(String key);

}
