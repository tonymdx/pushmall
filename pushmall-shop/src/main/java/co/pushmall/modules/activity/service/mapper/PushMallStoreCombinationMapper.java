package co.pushmall.modules.activity.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.activity.domain.PushMallStoreCombination;
import co.pushmall.modules.activity.service.dto.PushMallStoreCombinationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-11-18
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallStoreCombinationMapper extends EntityMapper<PushMallStoreCombinationDTO, PushMallStoreCombination> {

}
