package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallSystemStoreStaff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author pushmall
 * @date 2020-03-22
 */
public interface PushMallSystemStoreStaffRepository extends JpaRepository<PushMallSystemStoreStaff, Integer>, JpaSpecificationExecutor<PushMallSystemStoreStaff> {

    @Query(value = "SELECT  * FROM pushmall_system_store_staff WHERE uid = ?1 AND spread_uid = ?2", nativeQuery = true)
    List<PushMallSystemStoreStaff> findByUid(Integer uid, Integer spreadUid);
}
