package co.pushmall.modules.shop.service.mapper;


import co.pushmall.base.BaseMapper;
import co.pushmall.modules.shop.domain.PushMallMaterialGroup;
import co.pushmall.modules.shop.service.dto.PushMallMaterialGroupDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2020-01-09
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallMaterialGroupMapper extends BaseMapper<PushMallMaterialGroupDto, PushMallMaterialGroup> {

}
