package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallStoreProductAttrValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.math.BigDecimal;

/**
 * @author pushmall
 * @date 2019-10-13
 */
public interface PushMallStoreProductAttrValueRepository extends JpaRepository<PushMallStoreProductAttrValue, Integer>, JpaSpecificationExecutor {

    @Modifying
    @Transactional
    @Query(value = "delete from pushmall_store_product_attr_value where product_id =?1", nativeQuery = true)
    void deleteByProductId(Integer id);

    @Query(value = "select sum(stock)  from pushmall_store_product_attr_value " +
            "where product_id = ?1", nativeQuery = true)
    Integer sumStock(Integer productId);

    @Query(value = "select min(price)  from pushmall_store_product_attr_value " +
            "where product_id = ?1", nativeQuery = true)
    BigDecimal lowestPrice(Integer productId);

    @Query(value = "select *  from pushmall_store_product_attr_value t " +
            " where t.unique = ?1", nativeQuery = true)
    PushMallStoreProductAttrValue findbyUnique(String unique);

}
