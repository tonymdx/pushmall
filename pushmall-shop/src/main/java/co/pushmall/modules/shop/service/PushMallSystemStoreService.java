package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallSystemStore;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreDto;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreQueryCriteria;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2020-03-03
 */
public interface PushMallSystemStoreService {

    /**
     * 查询数据分页
     *
     * @param criteria 条件
     * @param pageable 分页参数
     * @return Map<String, Object>
     */
    Map<String, Object> queryAll(PushMallSystemStoreQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria 条件参数
     * @return List<PushMallSystemStoreDto>
     */
    List<PushMallSystemStoreDto> queryAll(PushMallSystemStoreQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return PushMallSystemStoreDto
     */
    PushMallSystemStoreDto findById(Integer id);

    /**
     * 创建
     *
     * @param resources /
     * @return PushMallSystemStoreDto
     */
    PushMallSystemStoreDto create(PushMallSystemStore resources);

    /**
     * 编辑
     *
     * @param resources /
     */
    void update(PushMallSystemStore resources);

    /**
     * 多选删除
     *
     * @param ids /
     */
    void deleteAll(Integer[] ids);

    /**
     * 导出数据
     *
     * @param all      待导出的数据
     * @param response /
     * @throws IOException /
     */
    void download(List<PushMallSystemStoreDto> all, HttpServletResponse response) throws IOException;
}
