package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallSystemGroupData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-10-18
 */
public interface PushMallSystemGroupDataRepository extends JpaRepository<PushMallSystemGroupData, Integer>, JpaSpecificationExecutor {
}
