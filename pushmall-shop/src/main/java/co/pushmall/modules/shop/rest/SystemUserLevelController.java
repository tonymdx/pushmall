package co.pushmall.modules.shop.rest;

import co.pushmall.aop.log.Log;
import co.pushmall.exception.BadRequestException;
import co.pushmall.modules.shop.domain.PushMallSystemUserLevel;
import co.pushmall.modules.shop.service.PushMallSystemUserLevelService;
import co.pushmall.modules.shop.service.PushMallUserService;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserLevelQueryCriteria;
import co.pushmall.modules.shop.service.dto.PushMallUserDTO;
import co.pushmall.utils.OrderUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author pushmall
 * @date 2019-12-04
 */
@Api(tags = "商城:用户等级管理")
@RestController
@RequestMapping("api")
public class SystemUserLevelController {

    private final PushMallSystemUserLevelService pushMallSystemUserLevelService;
    private final PushMallUserService pushMallUserService;

    public SystemUserLevelController(PushMallSystemUserLevelService pushMallSystemUserLevelService, PushMallUserService pushMallUserService) {
        this.pushMallSystemUserLevelService = pushMallSystemUserLevelService;
        this.pushMallUserService = pushMallUserService;
    }

    @Log("查询")
    @ApiOperation(value = "查询")
    @GetMapping(value = "/PmSystemUserLevel")
    @PreAuthorize("@el.check('admin','YXSYSTEMUSERLEVEL_ALL','YXSYSTEMUSERLEVEL_SELECT')")
    public ResponseEntity getPushMallSystemUserLevels(PushMallSystemUserLevelQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallSystemUserLevelService.queryAll(criteria, pageable), HttpStatus.OK);
    }

    @Log("新增")
    @ApiOperation(value = "新增")
    @PostMapping(value = "/PmSystemUserLevel")
    @PreAuthorize("@el.check('admin','YXSYSTEMUSERLEVEL_ALL','YXSYSTEMUSERLEVEL_CREATE')")
    public ResponseEntity create(@Validated @RequestBody PushMallSystemUserLevel resources) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        resources.setAddTime(OrderUtil.getSecondTimestampTwo());
        return new ResponseEntity(pushMallSystemUserLevelService.create(resources), HttpStatus.CREATED);
    }

    @Log("修改")
    @ApiOperation(value = "修改")
    @PutMapping(value = "/PmSystemUserLevel")
    @PreAuthorize("@el.check('admin','YXSYSTEMUSERLEVEL_ALL','YXSYSTEMUSERLEVEL_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallSystemUserLevel resources) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallSystemUserLevelService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除")
    @ApiOperation(value = "删除")
    @DeleteMapping(value = "/PmSystemUserLevel/{id}")
    @PreAuthorize("@el.check('admin','YXSYSTEMUSERLEVEL_ALL','YXSYSTEMUSERLEVEL_DELETE')")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        List<PushMallUserDTO> yxUser = pushMallUserService.findByLevel(id);
        if (yxUser != null && yxUser.size() > 0) {
            throw new BadRequestException("此会员级别已注册，不能删除!");
        } else {
            pushMallSystemUserLevelService.delete(id);
            return new ResponseEntity<>("删除成功!", HttpStatus.OK);
        }
    }
}
