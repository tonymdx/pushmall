package co.pushmall.modules.shop.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;

/**
 * @author pushmall
 * @date 2019-12-04
 */
@Data
public class PushMallSystemUserLevelQueryCriteria {
    @Query
    private Integer isDel;
}
